//
// Created by yakuza on 28.03.17.
//

#ifndef EMBED_MONO_MONOLOGICWRAPPER_H
#define EMBED_MONO_MONOLOGICWRAPPER_H

#include <iostream>
#include <string>
#include <mono/jit/jit.h>
#include <mono/metadata/assembly.h>
#include <mono/metadata/threads.h>
#include <functional>


std::string monoStringToStdString(MonoString *s);

class MonoLogicWrapper {
private:
    MonoDomain *_appDomain;
    MonoAssembly *_logicAssembly;
    MonoImage *_assemblyImage;
    MonoClass *_logicClass;

    MonoObject *_logicInstance;

    std::function<void(int, int, std::string, std::string)> _onPlayerMessageSent;

    MonoObject *getPropertyValue(MonoObject *instance, std::string propertyName);

    std::string getStringPropertyValue(MonoObject *instance, std::string propertyName);

    MonoObject *invokeMethod(std::string methodName, int argumentsCount, void *args[]);

    static void initHandlers();

public:
    MonoLogicWrapper(MonoDomain* rootDomain, std::string assemblyPath, std::string logicNamespace, std::string logicType);
    ~MonoLogicWrapper();

    bool init_logic(std::string configName);

    bool send_player_message(long timestamp, int user_id, int gamestate_id, std::string command_name,
                             std::string command_payload);

    void onPlayerMessageSent(std::function<void(int, int, std::string, std::string)> callback);

    void handle_send_message_from_logic(int user_id, int gamestate_id, std::string command_name,
                                        std::string command_payload);
};


#endif //EMBED_MONO_MONOLOGICWRAPPER_H
