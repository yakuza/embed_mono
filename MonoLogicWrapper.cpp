//
// Created by yakuza on 28.03.17.
//

#include "MonoLogicWrapper.h"
#include <thread>
#define LOGIC_PATH "../MonoSgsApi/LogicExampleMono/bin/Debug/LogicExampleMono.dll"
#define LOGIC_NAMESPACE "LogicExampleMono"
#define LOGIC_TYPE_NAME "MySgsGameLogic"
using namespace std;

void SGS_SendMessage(MonoLogicWrapper *logicWrapper, int userId, int gamestateId, MonoString *commandName,
                     MonoString *commandPayload) {
    logicWrapper->handle_send_message_from_logic(userId, gamestateId, monoStringToStdString(commandName),
                                                 monoStringToStdString(commandPayload));
}

std::string monoStringToStdString(MonoString *s) {
    char *message_text = mono_string_to_utf8(s);
    std::string result(message_text);
    mono_free(message_text);
    return result;
}

void MonoLogicWrapper::initHandlers() {
    mono_add_internal_call("SgsApi.ServerApi::SGS_SendMessage", (const void *) SGS_SendMessage);
}

MonoObject *MonoLogicWrapper::getPropertyValue(MonoObject *instance, std::string propertyName) {
    MonoClass *klass = mono_object_get_class(instance);
    MonoProperty *message_property = mono_class_get_property_from_name(klass, propertyName.c_str());
    MonoObject *property_value = mono_property_get_value(message_property, instance, NULL, NULL);

    return property_value;
}

std::string MonoLogicWrapper::getStringPropertyValue(MonoObject *instance, std::string propertyName) {

    return monoStringToStdString((MonoString *) getPropertyValue(instance, propertyName));
}

MonoObject *MonoLogicWrapper::invokeMethod(std::string methodName, int argumentsCount, void *args[]) {
    MonoObject *exc;

    MonoMethod *call_me_method = mono_class_get_method_from_name(_logicClass,
                                                                 methodName.c_str(), argumentsCount);

    MonoObject *result = mono_runtime_invoke(call_me_method, _logicInstance, args, &exc);

    if (exc) {
        std::cout << "Caught exception " << mono_class_get_name(mono_object_get_class(exc)) << ": "
                  << getStringPropertyValue(exc, "Message") << std::endl <<
                  getStringPropertyValue(exc, "StackTrace") << std::endl;

        MonoObject *inner = getPropertyValue(exc, "InnerException");
        if (inner) {
            std::cout << "\tInner exception " << mono_class_get_name(mono_object_get_class(exc)) << ": "
                      << getStringPropertyValue(exc, "Message") << std::endl << "\t" <<
                      getStringPropertyValue(exc, "StackTrace") << std::endl;
        }

        throw "FAILED";
    }

    return result;
}

MonoLogicWrapper::MonoLogicWrapper(MonoDomain* rootDomain, string _, string __, string ___){
    mono_thread_attach(rootDomain);
    auto assemblyPath = LOGIC_PATH;
    auto logicNamespace = LOGIC_NAMESPACE;
    auto logicType = LOGIC_TYPE_NAME;
    _appDomain = mono_domain_create();
    _logicAssembly = mono_domain_assembly_open(_appDomain, assemblyPath);
    if (!_logicAssembly) {
        mono_jit_cleanup(_appDomain);
        throw "Failed to load assembly!";
    }

    _assemblyImage = mono_assembly_get_image(_logicAssembly);
    _logicClass = mono_class_from_name(_assemblyImage, logicNamespace, logicType);
    mono_thread_attach(mono_get_root_domain());
    _logicInstance = mono_object_new(_appDomain, _logicClass);
    void *args[1];

    int64_t this_pointer = reinterpret_cast<int64_t>(this);
    args[0] = &this_pointer;
    invokeMethod(".ctor", 1, args);

    initHandlers();
}

MonoLogicWrapper::~MonoLogicWrapper() {
    cout << "Tearing down app domain..." << endl;
    mono_domain_finalize(_appDomain, 1000);
    cout << "Unloading domain..." << endl;
    mono_domain_free(_appDomain, 0);
    //mono_domain_unload(_appDomain);
    cout << "Done!" << endl;
}

bool MonoLogicWrapper::init_logic(std::string configName) {
    void *args[1];
    MonoString *arg = mono_string_new(_appDomain, configName.c_str());
    args[0] = arg;

    MonoObject *result = invokeMethod("InitLogic", 1, args);

    return *(bool *) result;
}

bool MonoLogicWrapper::send_player_message(long timestamp, int user_id, int gamestate_id, std::string command_name,
                                           std::string command_payload) {

    void *args[5];
    args[0] = &timestamp;
    args[1] = &user_id;
    args[2] = &gamestate_id;
    args[3] = mono_string_new(_appDomain, command_name.c_str());
    args[4] = mono_string_new(_appDomain, command_payload.c_str());

    MonoObject *result = invokeMethod("HandlePlayerMessage", 5, args);

    return *(bool *) result;
}

void MonoLogicWrapper::onPlayerMessageSent(std::function<void(int, int, std::string, std::string)> callback) {
    this->_onPlayerMessageSent = callback;
}

void MonoLogicWrapper::handle_send_message_from_logic(int userId, int gamestateId, std::string commandName,
                                                      std::string commandPayload) {
    _onPlayerMessageSent(userId, gamestateId, commandName, commandPayload);
}

