#include <iostream>
#include <thread>

#include "MonoLogicWrapper.h"

#define LOGIC_PATH "../MonoSgsApi/LogicExampleMono/bin/Debug/LogicExampleMono.dll"
#define LOGIC_NAMESPACE "LogicExampleMono"
#define LOGIC_TYPE_NAME "MySgsGameLogic"
#define THREADS_NUM 5

using namespace std;

auto rootDomain = mono_jit_init("default");

void print_thread_message(int threadId, string message) {
    cout << "[" << this_thread::get_id() << "][" << threadId << "]::" << message << endl;
}

void runGameLogic(int threadId) {
    print_thread_message(threadId, "Starting thread #" + to_string(threadId));
    try {
        function<void(int, int, string, string)> blabla = [threadId](int userId, int gamestateId,
                                                             string commandName,
                                                             string commandPayload) -> void {
            print_thread_message(threadId, "Received call from Mono: (" + to_string(userId) + ", " +
                                 to_string(gamestateId) + ") " + commandName + " || " +
                                 commandPayload);
        };
        MonoLogicWrapper logic(rootDomain, "../MonoSgsApi/LogicExampleMono/bin/Debug/LogicExampleMono.dll", "LogicExampleMono", "MySgsGameLogic");

        string configName = "T#" + to_string(threadId) + "Config name";

        logic.init_logic(configName);
        logic.onPlayerMessageSent(blabla);
        logic.send_player_message(12345 * threadId, threadId, threadId, "get_get", "blabla ble ble");
        logic.send_player_message(12345 * threadId, threadId, threadId, "get_config", "");
    } catch (const char *msg) {
        print_thread_message(threadId, "Unhandled error - details should be logged above.");
    }
}

int main() {

    std::thread threads[THREADS_NUM];

    for (int i = 0; i < THREADS_NUM; ++i) {
        threads[i] = std::thread(runGameLogic, i);
    }

    for (int i = 0; i < THREADS_NUM; ++i) {
        threads[i].join();
    }
    mono_jit_cleanup(rootDomain);
    return 0;
}