﻿namespace SgsApi
{
	public interface IGameLogic
	{
		bool InitLogic(string configName);
		bool HandlePlayerMessage(long messageTimestamp, int userId, int gamestateId, string commandName, string commandPayload);
		bool PutGamestate(long timestamp, int gamestateId, string data);
		string GetGamestate(int gamestateId, bool serverClosing, bool logicReload);
		void EraseGamestate(int gamestateId);
	}
}
