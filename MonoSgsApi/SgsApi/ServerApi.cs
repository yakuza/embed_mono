﻿using System;
using System.Runtime.CompilerServices;

namespace SgsApi
{
	public class ServerApi
	{
		private readonly long _logicWrapperHandle;

		[MethodImplAttribute(MethodImplOptions.InternalCall)]
		static extern void SGS_SendMessage(long unmanagedHandle, int userId, int gamestateId, string commandName, string commandPayload);

		public ServerApi(long logicWrapperHandle)
		{
			_logicWrapperHandle = logicWrapperHandle;
		}

		public void SendMessage(int userId, int gamestateId, string commandName, string commandPayload)
		{
			ServerApi.SGS_SendMessage(_logicWrapperHandle, userId, gamestateId, commandName, commandPayload);
		}
	}
}
