﻿using System;
using System.Collections.Generic;
using SgsApi;
using System.Threading;

namespace LogicExampleMono
{
	public class MySgsGameLogic : IGameLogic
	{
		private readonly Dictionary<int, string> _gamestates = new Dictionary<int, string>();
		private string _configName = "";
		private readonly ServerApi _api;

		public MySgsGameLogic(long serverApiHandle)
		{
			_api = new ServerApi(serverApiHandle);
		}

		private void Print(string message, params object[] args)
		{
			string formattedMessage = string.Format("[MONO][T#{0}] {1}", Thread.CurrentThread.ManagedThreadId, string.Format(message, args));

			Console.WriteLine(formattedMessage);
		}

		public void EraseGamestate(int gamestateId)
		{
			Print("[ERASE_GAMESTATE][{0}]", gamestateId);
		}

		public string GetGamestate(int gamestateId, bool serverClosing, bool logicReload)
		{
			Print("[GET_GAMESTATE][{0}] serverClosing: {1}, logicReload: {2}", gamestateId, serverClosing, logicReload);
			return _gamestates.ContainsKey(gamestateId) ? _gamestates[gamestateId] : "";
		}

		public bool HandlePlayerMessage(long messageTimestamp, int userId, int gamestateId, string commandName, string commandPayload)
		{
			Print("[HANDLE_PLAYER_MESSAGE][{0}]({1}, {2}) Command: {3} || Payload: {4}", messageTimestamp, userId, gamestateId, commandName, commandPayload);

			if (commandName == "get_config")
			{
				Print(string.Format("Sending reply: {0}, {1}, {2}, {3}", userId, gamestateId, commandName, _configName));
				try
				{
					_api.SendMessage(userId, gamestateId, commandName, _configName);
				}
				catch (Exception e)
				{
					throw;
					//Console.WriteLine(e);
					//Console.WriteLine(e.StackTrace);
				}
			}

			return true;
		}

		public bool InitLogic(string configName)
		{
			_configName = configName;
			Print("[INIT_LOGIC] Config name: {0}", configName);
			return true;
		}

		public bool PutGamestate(long timestamp, int gamestateId, string data)
		{
			Print("[PUT_GAMESTATE][{0}]({1}) Gamestate: {2}", timestamp, gamestateId, data);
			_gamestates[gamestateId] = data;
			return true;
		}
	}
}
